let mymap = L.map('map').setView([47.240092, 39.710652], 16);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibGFua2FhIiwiYSI6ImNqbWJ1bmw3cTB3dXMzcXMzMGJuN2dvdGoifQ.KvQKbo0dio7nPNBn9NyqTQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

let park = L.marker([47.240092, 39.710652]).addTo(mymap);

park.bindPopup("<b>Парк ДГТУ</b>");


let btnMap = document.getElementById('toMap');
btnMap.addEventListener('click', toMap);

function toMap() {
    window.location.href = 'map.html';
}
