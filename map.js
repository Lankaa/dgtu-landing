/*
////////////////////////////////////////////////
MAP
////////////////////////////////////////////////
*/

let fullMap = L.map('fullMap').setView([47.240092, 39.710652], 16);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibGFua2FhIiwiYSI6ImNqbWJ1bmw3cTB3dXMzcXMzMGJuN2dvdGoifQ.KvQKbo0dio7nPNBn9NyqTQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'your.mapbox.access.token'
}).addTo(fullMap);

let Points = L.Icon.extend({
    options: {
        iconSize:     [30, 30],
        iconAnchor:   [10, 29],
        popupAnchor:  [4, -23]
    }
});


let parkIcon = new Points({iconUrl: 'img/park-icon.png'});
let churchIcon = new Points({iconUrl: 'img/church.png'});
let monumentIcon = new Points({iconUrl: 'img/monument-icon.png'});
let hostelIcon = new Points({iconUrl: 'img/hostel-icon.png'});
let busstopIcon = new Points({iconUrl: 'img/busstop-icon.png'});
let gymIcon = new Points({iconUrl: 'img/gym-icon.png'});

L.icon = function (options) {
    return new L.Icon(options);
};


let parkMarker = L.marker([47.240092, 39.710652], {icon: parkIcon})
    .addTo(fullMap).bindPopup("<b>Парк ДГТУ</b>");
let churchMarker = L.marker([47.239277, 39.710983], {icon: churchIcon})
    .addTo(fullMap).bindPopup("<b>Храм святой Татьяны</b>" +
        "<br>");
let monumentMarker = L.marker([47.239274, 39.710363], {icon: monumentIcon})
    .addTo(fullMap).bindPopup("<b>Памятник</b>" +
        "<br>Студентам и преподавателям РИСХМА");
let hostelMarker = L.marker([47.239388, 39.712745], {icon: hostelIcon})
    .addTo(fullMap).bindPopup("<b>Общежитие ДГТУ</b>");
let busstopMarker = L.marker([47.238753, 39.713301], {icon: busstopIcon})
    .addTo(fullMap).bindPopup("<b>Остановка ДГТУ</b>" +
        "<br>Автобусы: 22, 33, 43");
let gymMarker = L.marker([47.240923, 39.709388], {icon: gymIcon})
    .addTo(fullMap).bindPopup("<b>Спортзал ДГТУ</b>" +
        "<br>Легко-атлетический манеж ДГТУ");

L.control.mousePosition().addTo(fullMap);

L.Control.measureControl().addTo(fullMap);




/*
/////////////////////////////////////////////
LIST OF PLACES
/////////////////////////////////////////////
*/

const collection = new Backbone.Collection([
    {
        name: 'Памятник',
        description: '',
        lat: 47.239274,
        lng: 39.710363,
        marker() {
            monumentMarker.openPopup();
        }
    },
    {
        name: 'Парк ДГТУ',
        description: 'Студенческий парк ДГТУ',
        lat: 47.240092,
        lng: 39.710652,
        marker() {
            parkMarker.openPopup();
        }
    },
    {
        name: 'Храм',
        description: 'Храм святой мученицы Татьяны при ДГТУ',
        lat: 47.239277,
        lng: 39.710983,
        marker() {
            churchMarker.openPopup();
        }
    },
    {
        name: 'Общежитие ДГТУ',
        description: '',
        lat: 47.239388,
        lng: 39.712745,
        marker() {
            hostelMarker.openPopup();
        }
    },
    {
        name: 'Спортивный зал',
        description: '',
        lat: 47.240923,
        lng: 39.709388,
        marker() {
            gymMarker.openPopup();
        }
    },
    {
        name: 'Остановка ДГТУ',
        description: '22, 33, 42',
        lat: 47.238753,
        lng: 39.713301,
        marker() {
            busstopMarker.openPopup();
        }
    }
]);

const Cards = Mn.View.extend({
    initialize: function(options) {
        this.index = options.childIndex;
    },
    template: _.template(`
  <div id="card" class="card">
        <div class="card-header">
            <%= name %>
        </div>
        <div class="card-body">
            <p><%= description%></p>
        </div>
    </div>
  `),
    events: {
        'click': 'onClick'
    },
    onClick() {
        let view = cardView.children.findByIndex(this.index);
        let attr = view.model.attributes;

        fullMap.setView([attr.lat, attr.lng], 16);
        attr.marker();

    }

});

const CardsList = Mn.CollectionView.extend({
    el: '#card_wrapper',
    childView: Cards,
    childViewOptions: function(model) {
        return{
            childIndex: this.collection.indexOf(model)
        }
    }
});

const cardView = new CardsList({collection});
cardView.render();


